# GitLab

GitLab is a project where I host GitLab behind a Traefik reverse-proxy.

# Installation

[Install Docker Compose](https://docs.docker.com/compose/install/).

# Running

```shell
docker-compose up
```
